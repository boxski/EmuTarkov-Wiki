## Frequently Asked Questions

### The launcher closes as soon as it opens
Copy-paste `Newtonsoft.Json.dll` and `zlib.net.dll` from `EscapeFromTarkov_Data/Managed/` into the same directory where `EmuTarkov-Launcher.exe` is located.

### The launcher reports that no servers are available
Make sure that `EmuTarkov-Server.exe` says that the server is running. After this, press the refresh button.

### The launcher reports that no connection can be established
Make sure that `EmuTarkov-Server.exe` says that the server is running.

### Where is the mods folder located?
In `user/mods/`. If it's not located there, create a folder `mods` in the `user` folder of the server.

### Where is the `bots_f` folder located?
[the reference](https://discordapp.com/channels/681163523975675914/682164162704703488/682954979325247505)

<img src="https://i.ytimg.com/vi/nUj4ObkwrkY/maxresdefault.jpg" width="130">
