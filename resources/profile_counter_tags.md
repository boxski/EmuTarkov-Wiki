# CHARACTER IN GAME COUNTER
```
{
		// Token: 0x0400695B RID: 26971
		LootItem,
		// Token: 0x0400695C RID: 26972
		HeadShots,
		// Token: 0x0400695D RID: 26973
		BloodLoss,
		// Token: 0x0400695E RID: 26974
		BodyPartsDestroyed,
		// Token: 0x0400695F RID: 26975
		BodyPartDamage,
		// Token: 0x04006960 RID: 26976
		Heal,
		// Token: 0x04006961 RID: 26977
		Fractures,
		// Token: 0x04006962 RID: 26978
		Contusions,
		// Token: 0x04006963 RID: 26979
		Dehydrations,
		// Token: 0x04006964 RID: 26980
		Exhaustions,
		// Token: 0x04006965 RID: 26981
		Medicines,
		// Token: 0x04006966 RID: 26982
		UsedFoods,
		// Token: 0x04006967 RID: 26983
		UsedDrinks,
		// Token: 0x04006968 RID: 26984
		TriggerVisited,
		// Token: 0x04006969 RID: 26985
		Triggers,
		// Token: 0x0400696A RID: 26986
		Pedometer,
		// Token: 0x0400696B RID: 26987
		Money,
		// Token: 0x0400696C RID: 26988
		BodiesLooted,
		// Token: 0x0400696D RID: 26989
		SafeLooted,
		// Token: 0x0400696E RID: 26990
		LockableContainers,
		// Token: 0x0400696F RID: 26991
		Weapons,
		// Token: 0x04006970 RID: 26992
		Ammunitions,
		// Token: 0x04006971 RID: 26993
		Mods,
		// Token: 0x04006972 RID: 26994
		ThrowWeapons,
		// Token: 0x04006973 RID: 26995
		SpecialItems,
		// Token: 0x04006974 RID: 26996
		FoodDrinks,
		// Token: 0x04006975 RID: 26997
		Keys,
		// Token: 0x04006976 RID: 26998
		BartItems,
		// Token: 0x04006977 RID: 26999
		MobContainers,
		// Token: 0x04006978 RID: 27000
		Equipments,
		// Token: 0x04006979 RID: 27001
		CauseBodyDamage,
		// Token: 0x0400697A RID: 27002
		CauseArmorDamage,
		// Token: 0x0400697B RID: 27003
		HitCount,
		// Token: 0x0400697C RID: 27004
		LongShots,
		// Token: 0x0400697D RID: 27005
		LongestShot,
		// Token: 0x0400697E RID: 27006
		LongestKillShot,
		// Token: 0x0400697F RID: 27007
		CurrentWinStreak,
		// Token: 0x04006980 RID: 27008
		LongestWinStreak,
		// Token: 0x04006981 RID: 27009
		Sessions,
		// Token: 0x04006982 RID: 27010
		ExitStatus,
		// Token: 0x04006983 RID: 27011
		Pmc,
		// Token: 0x04006984 RID: 27012
		Scav,
		// Token: 0x04006985 RID: 27013
		LifeTime,
		// Token: 0x04006986 RID: 27014
		AmmoUsed,
		// Token: 0x04006987 RID: 27015
		AmmoReached,
		// Token: 0x04006988 RID: 27016
		Deaths,
		// Token: 0x04006989 RID: 27017
		KilledLevel0010,
		// Token: 0x0400698A RID: 27018
		KilledLevel1030,
		// Token: 0x0400698B RID: 27019
		KilledLevel3050,
		// Token: 0x0400698C RID: 27020
		KilledLevel5070,
		// Token: 0x0400698D RID: 27021
		KilledLevel7099,
		// Token: 0x0400698E RID: 27022
		KilledLevel100,
		// Token: 0x0400698F RID: 27023
		KilledBear,
		// Token: 0x04006990 RID: 27024
		KilledUsec,
		// Token: 0x04006991 RID: 27025
		KilledSavage,
		// Token: 0x04006992 RID: 27026
		KilledPmc,
		// Token: 0x04006993 RID: 27027
		KilledBoss,
		// Token: 0x04006994 RID: 27028
		LongestKillStreak,
		// Token: 0x04006995 RID: 27029
		KilledWithKnife,
		// Token: 0x04006996 RID: 27030
		KilledWithPistol,
		// Token: 0x04006997 RID: 27031
		KilledWithSmg,
		// Token: 0x04006998 RID: 27032
		KilledWithShotgun,
		// Token: 0x04006999 RID: 27033
		KilledWithAssaultRifle,
		// Token: 0x0400699A RID: 27034
		KilledWithAssaultCarbine,
		// Token: 0x0400699B RID: 27035
		KilledWithGrenadeLauncher,
		// Token: 0x0400699C RID: 27036
		KilledWithMachineGun,
		// Token: 0x0400699D RID: 27037
		KilledWithMarksmanRifle,
		// Token: 0x0400699E RID: 27038
		KilledWithSniperRifle,
		// Token: 0x0400699F RID: 27039
		KilledWithSpecialWeapon,
		// Token: 0x040069A0 RID: 27040
		KilledWithThrowWeapon,
		// Token: 0x040069A1 RID: 27041
		Kills,
		// Token: 0x040069A2 RID: 27042
		Exp,
		// Token: 0x040069A3 RID: 27043
		ExpKill,
		// Token: 0x040069A4 RID: 27044
		ExpLooting,
		// Token: 0x040069A5 RID: 27045
		ExpKillBase,
		// Token: 0x040069A6 RID: 27046
		ExpKillBodyPartBonus,
		// Token: 0x040069A7 RID: 27047
		ExpKillStreakBonus,
		// Token: 0x040069A8 RID: 27048
		ExpDamage,
		// Token: 0x040069A9 RID: 27049
		ExpItemLooting,
		// Token: 0x040069AA RID: 27050
		ExpContainerLooting,
		// Token: 0x040069AB RID: 27051
		ExpDoorUnlocked,
		// Token: 0x040069AC RID: 27052
		ExpDoorBreached,
		// Token: 0x040069AD RID: 27053
		ExpHeal,
		// Token: 0x040069AE RID: 27054
		ExpEnergy,
		// Token: 0x040069AF RID: 27055
		ExpHydration,
		// Token: 0x040069B0 RID: 27056
		ExpExitStatus,
		// Token: 0x040069B1 RID: 27057
		ExpTrigger,
		// Token: 0x040069B2 RID: 27058
		ExpStationaryContainer,
		// Token: 0x040069B3 RID: 27059
		ExpExamine
	}```