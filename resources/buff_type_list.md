# ALL BUFF TYPES LIST
```
{
		// Token: 0x040052B9 RID: 21177
		None,
		// Token: 0x040052BA RID: 21178
		EnduranceBuffEnduranceInc,
		// Token: 0x040052BB RID: 21179
		EnduranceBuffJumpCostRed,
		// Token: 0x040052BC RID: 21180
		EnduranceBuffBreathTimeInc,
		// Token: 0x040052BD RID: 21181
		EnduranceBuffRestorationTimeRed,
		// Token: 0x040052BE RID: 21182
		EnduranceBreathElite,
		// Token: 0x040052BF RID: 21183
		StrengthBuffLiftWeightInc,
		// Token: 0x040052C0 RID: 21184
		StrengthBuffSprintSpeedInc,
		// Token: 0x040052C1 RID: 21185
		StrengthBuffJumpHeightInc,
		// Token: 0x040052C2 RID: 21186
		StrengthBuffThrowDistanceInc,
		// Token: 0x040052C3 RID: 21187
		StrengthBuffMeleePowerInc,
		// Token: 0x040052C4 RID: 21188
		StrengthBuffElite,
		// Token: 0x040052C5 RID: 21189
		StrengthBuffMeleeCrits,
		// Token: 0x040052C6 RID: 21190
		VitalityBuffBleedChanceRed,
		// Token: 0x040052C7 RID: 21191
		VitalityBuffSurviobilityInc,
		// Token: 0x040052C8 RID: 21192
		VitalityBuffRegeneration,
		// Token: 0x040052C9 RID: 21193
		VitalityBuffBleedStop,
		// Token: 0x040052CA RID: 21194
		HealthBreakChanceRed,
		// Token: 0x040052CB RID: 21195
		HealthOfflineRegenerationInc,
		// Token: 0x040052CC RID: 21196
		HealthEnergy,
		// Token: 0x040052CD RID: 21197
		HealthHydration,
		// Token: 0x040052CE RID: 21198
		HealthEliteAbsorbDamage,
		// Token: 0x040052CF RID: 21199
		HealthElitePosion,
		// Token: 0x040052D0 RID: 21200
		StressPainChance,
		// Token: 0x040052D1 RID: 21201
		StressTremor,
		// Token: 0x040052D2 RID: 21202
		StressBerserk,
		// Token: 0x040052D3 RID: 21203
		MetabolismRatioPlus,
		// Token: 0x040052D4 RID: 21204
		MetabolismEnergyExpenses,
		// Token: 0x040052D5 RID: 21205
		MetabolismPhysicsForget,
		// Token: 0x040052D6 RID: 21206
		MetabolismPhysicsForget2,
		// Token: 0x040052D7 RID: 21207
		MetabolismEliteBuffNoDyhydration,
		// Token: 0x040052D8 RID: 21208
		MetabolismEliteNoForget,
		// Token: 0x040052D9 RID: 21209
		PerceptionHearing,
		// Token: 0x040052DA RID: 21210
		PerceptionFov,
		// Token: 0x040052DB RID: 21211
		PerceptionLootDot,
		// Token: 0x040052DC RID: 21212
		PerceptionmEliteNoIdea,
		// Token: 0x040052DD RID: 21213
		IntellectLearningSpeed,
		// Token: 0x040052DE RID: 21214
		IntellectWeaponMaintance,
		// Token: 0x040052DF RID: 21215
		IntellectEliteNaturalLearner,
		// Token: 0x040052E0 RID: 21216
		IntellectEliteAmmoCounter,
		// Token: 0x040052E1 RID: 21217
		IntellectEliteContainerScope,
		// Token: 0x040052E2 RID: 21218
		AttentionLootSpeed,
		// Token: 0x040052E3 RID: 21219
		AttentionRareLoot,
		// Token: 0x040052E4 RID: 21220
		AttentionEliteLuckySearch,
		// Token: 0x040052E5 RID: 21221
		AttentionEliteExtraLootExp,
		// Token: 0x040052E6 RID: 21222
		MagDrillsLoadSpeed,
		// Token: 0x040052E7 RID: 21223
		MagDrillsUnloadSpeed,
		// Token: 0x040052E8 RID: 21224
		MagDrillsInventoryCheckSpeed,
		// Token: 0x040052E9 RID: 21225
		MagDrillsInventoryCheckAccuracy,
		// Token: 0x040052EA RID: 21226
		MagDrillsInstantCheck,
		// Token: 0x040052EB RID: 21227
		MagDrillsLoadProgression,
		// Token: 0x040052EC RID: 21228
		CharismaBuff1,
		// Token: 0x040052ED RID: 21229
		CharismaBuff2,
		// Token: 0x040052EE RID: 21230
		CharismaEliteBuff1,
		// Token: 0x040052EF RID: 21231
		CharismaEliteBuff2,
		// Token: 0x040052F0 RID: 21232
		MemoryMentalForget1,
		// Token: 0x040052F1 RID: 21233
		MemoryMentalForget2,
		// Token: 0x040052F2 RID: 21234
		MemoryEliteMentalNoDegradation,
		// Token: 0x040052F3 RID: 21235
		WeaponReloadBuff,
		// Token: 0x040052F4 RID: 21236
		WeaponRecoilBuff,
		// Token: 0x040052F5 RID: 21237
		WeaponSwapBuff,
		// Token: 0x040052F6 RID: 21238
		DrawMasterSpeed,
		// Token: 0x040052F7 RID: 21239
		DrawMasterElite,
		// Token: 0x040052F8 RID: 21240
		AimMasterElite,
		// Token: 0x040052F9 RID: 21241
		AimMasterWiggle,
		// Token: 0x040052FA RID: 21242
		AimMasterSpeed,
		// Token: 0x040052FB RID: 21243
		RecoilControlImprove,
		// Token: 0x040052FC RID: 21244
		RecoilControlElite,
		// Token: 0x040052FD RID: 21245
		CovertMovementSoundVolume,
		// Token: 0x040052FE RID: 21246
		ProneMovementSpeed,
		// Token: 0x040052FF RID: 21247
		ProneMovementVolume,
		// Token: 0x04005300 RID: 21248
		ProneMovementElite,
		// Token: 0x04005301 RID: 21249
		TroubleFixing,
		// Token: 0x04005302 RID: 21250
		TroubleFixingElite,
		// Token: 0x04005303 RID: 21251
		WeaponErgonomicsBuff,
		// Token: 0x04005304 RID: 21252
		WeaponDoubleMastering,
		// Token: 0x04005305 RID: 21253
		WeaponStiffHands,
		// Token: 0x04005306 RID: 21254
		ThrowingStrengthBuff,
		// Token: 0x04005307 RID: 21255
		ThrowingEnergyExpenses,
		// Token: 0x04005308 RID: 21256
		ThrowingWeaponsBuffElite,
		// Token: 0x04005309 RID: 21257
		CovertMovementSpeed,
		// Token: 0x0400530A RID: 21258
		CovertMovementElite,
		// Token: 0x0400530B RID: 21259
		CovertMovementLoud,
		// Token: 0x0400530C RID: 21260
		CovertMovementEquipment,
		// Token: 0x0400530D RID: 21261
		SearchSpeed,
		// Token: 0x0400530E RID: 21262
		SearchDouble
	}
	```

# STIMULATOR BUFF LIST

```
{
		// Token: 0x04006C12 RID: 27666
		HealthRate,
		// Token: 0x04006C13 RID: 27667
		EnergyRate,
		// Token: 0x04006C14 RID: 27668
		HydrationRate,
		// Token: 0x04006C15 RID: 27669
		SkillRate,
		// Token: 0x04006C16 RID: 27670
		MaxStamina,
		// Token: 0x04006C17 RID: 27671
		StaminaRate,
		// Token: 0x04006C18 RID: 27672
		StomachBloodloss,
		// Token: 0x04006C19 RID: 27673
		ContusionBlur,
		// Token: 0x04006C1A RID: 27674
		ContusionWiggle,
		// Token: 0x04006C1B RID: 27675
		Pain,
		// Token: 0x04006C1C RID: 27676
		HandsTremor,
		// Token: 0x04006C1D RID: 27677
		QuantumTunnelling,
		// Token: 0x04006C1E RID: 27678
		RemoveNegativeEffects,
		// Token: 0x04006C1F RID: 27679
		RemoveAllBuffs,
		// Token: 0x04006C20 RID: 27680
		RemoveAllBloodLosses
	}
```
