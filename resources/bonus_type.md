# ALL PROFILE BONUS TYPES
```
{
		// Token: 0x0400466D RID: 18029
		EnergyRegeneration,
		// Token: 0x0400466E RID: 18030
		HydrationRegeneration,
		// Token: 0x0400466F RID: 18031
		HealthRegeneration,
		// Token: 0x04004670 RID: 18032
		MaximumEnergyReserve,
		// Token: 0x04004671 RID: 18033
		ExperienceRate,
		// Token: 0x04004672 RID: 18034
		QuestMoneyReward,
		// Token: 0x04004673 RID: 18035
		ScavCooldownTimer,
		// Token: 0x04004674 RID: 18036
		StashSize,
		// Token: 0x04004675 RID: 18037
		UnlockItemCraft,
		// Token: 0x04004676 RID: 18038
		UnlockItemPassiveCreation,
		// Token: 0x04004677 RID: 18039
		UnlockRandomItemCreation,
		// Token: 0x04004678 RID: 18040
		SkillGroupLevelingBoost,
		// Token: 0x04004679 RID: 18041
		SkillLevelingBoost,
		// Token: 0x0400467A RID: 18042
		DebuffEndDelay,
		// Token: 0x0400467B RID: 18043
		RagfairCommission,
		// Token: 0x0400467C RID: 18044
		InsuranceReturnTime,
		// Token: 0x0400467D RID: 18045
		UnlockWeaponModification,
		// Token: 0x0400467E RID: 18046
		UnlockScavPlay,
		// Token: 0x0400467F RID: 18047
		UnlockAddOffer,
		// Token: 0x04004680 RID: 18048
		UnlockItemCharge,
		// Token: 0x04004681 RID: 18049
		ReceiveItemBonus,
		// Token: 0x04004682 RID: 18050
		UnlockUniqueId,
		// Token: 0x04004683 RID: 18051
		IncreaseCanisterSlots,
		// Token: 0x04004684 RID: 18052
		AdditionalSlots,
		// Token: 0x04004685 RID: 18053
		FuelConsumption,
		// Token: 0x04004686 RID: 18054
		TextBonus
	}```