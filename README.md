# JustEmuTarkov Wiki FanMade

You can find here various tutorial and most of the servers functions !

**You can find the EmuTarkov faq [HERE](https://github.com/justemutarkov/Wiki/blob/master/FAQ.md)**

## Tutorials
* [How to report bugs](https://github.com/justemutarkov/Wiki/blob/master/tutorials/bug-report.md)
* [How to setup your development environment](https://github.com/justemutarkov/Wiki/blob/master/tutorials/development-environment-setup.md)
* [How to create a mod](https://github.com/justemutarkov/Wiki/blob/master/tutorials/create_a_mod.md)
* [How to changes items textures](https://github.com/justemutarkov/Wiki/blob/master/tutorials/edit_weapons_texture.md)
* [How to create weather](https://github.com/justemutarkov/Wiki/blob/master/tutorials/create_weather.md)
* [How to edit various player related things (Skills, level, quests, hideout, traders standing)](https://github.com/justemutarkov/Wiki/blob/master/tutorials/edit_the_player_profile.md)
